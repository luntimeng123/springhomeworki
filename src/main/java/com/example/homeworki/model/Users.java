package com.example.homeworki.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Users {

    @Id
    @Column(name="u_id")
    private int userId;

    @Column(name="username")
    private String userName;
    @JsonIgnore // to hide this field when display
    @Column(name="password")
    private String passWord;

    @OneToMany(mappedBy = "users",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonBackReference
    private List<Article> articles;


    @ManyToMany()
    @JsonManagedReference
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id",referencedColumnName = "u_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",referencedColumnName = "r_id")
    )
    private List<Roles> roles;

//    @OneToMany(mappedBy = "users" ,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//    @JsonBackReference
//    private List<UsersRoles> usersRoles;

}
