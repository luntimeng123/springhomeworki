package com.example.homeworki.model;


import com.example.homeworki.payload.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Table(name="article")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "a_id")
    private int articleId;

    @Column(name="a_title")
    private String Title;
    @Column(name="a_description")
    private String Description;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "category_id",referencedColumnName = "c_id")
    private Category category;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "users_id",referencedColumnName = "u_id")
    private Users users;

}
