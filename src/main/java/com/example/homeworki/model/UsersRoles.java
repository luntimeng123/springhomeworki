package com.example.homeworki.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@Entity
//@Table(name = "users_roles")
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//public class UsersRoles {
//
//    @Id
//    @Column(name = "users_roles")
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private int usersRolesId;
//
//    @ManyToOne
//    @JoinColumn(name = "users_id",referencedColumnName = "u_id")
//    @JsonManagedReference
//    private Users users;
//
//    @ManyToOne
//    @JoinColumn(name = "roles_id",referencedColumnName = "r_id")
//    @JsonManagedReference
//    private Roles roles;
//}
