package com.example.homeworki.controller;




import com.example.homeworki.model.Article;
import com.example.homeworki.model.Category;
import com.example.homeworki.model.Users;
import com.example.homeworki.payload.criteria.ArticleCriteria;
import com.example.homeworki.payload.request.ArticleRequest;
import com.example.homeworki.repository.ArticleRepository;
import com.example.homeworki.repository.CategoryRepository;
import com.example.homeworki.repository.UserRepository;
import com.example.homeworki.repository.spec.AritcleSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.persistence.criteria.CriteriaBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("/api/v1") public class ArticleRestController
{

    @Autowired private ArticleRepository articleRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private CategoryRepository categoryRepository;


    // fetch article by specific user
    @GetMapping("/article/user/{userid}") public ResponseEntity<Map<String, Object>> getArticleBySpecificUser(
           @PathVariable  int userid,
           @RequestParam int page
    ){
            Pageable pageable = PageRequest.of(page,3);
            try {
                Map map = new HashMap();
                map.put("message","success to fetch article!");
                map.put("code",200);
                map.put("status","OK");
                map.put("data",articleRepository.findArticleByUsersUserId(userid,pageable).getContent());
                map.put("pageable",articleRepository.findArticleByUsersUserId(userid,pageable).getPageable());
                return ResponseEntity.ok().body(map);
            }catch (Exception e){
                Map map = new HashMap();
                map.put("message","fails to fetch article!");
                map.put("code",400);
                map.put("status","BadRequest");
                return ResponseEntity.badRequest().body(map);
            }
    }

    // search article
    @GetMapping("/article/search/{categoryid}") public ResponseEntity<Map<String, Object>> searchArticle(
            @PathVariable long categoryid,
            @RequestParam String username,
            @RequestParam Optional<Integer> pagenumber
    ){
        if (pagenumber.isEmpty()) pagenumber = Optional.of(0);
        try {
            Pageable pageable = PageRequest.of(pagenumber.get(),3);
            ArticleCriteria articleCriteria = new ArticleCriteria();
            articleCriteria.setCetegoryId(categoryid);
            articleCriteria.setUserName(username);
            AritcleSpecification aritcleSpecification = new AritcleSpecification(articleCriteria);
            Map map = new HashMap();
            map.put("message","success to fetch article!");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",articleRepository.findAll(aritcleSpecification,pageable).getContent());
            map.put("pageable",articleRepository.findAll(aritcleSpecification,pageable).getPageable());
            return ResponseEntity.ok().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to fetch article!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }


    // fetch article by catefory
    @GetMapping("/article/category/{cateid}") public ResponseEntity<Map<String, Object>> getArticleBySpecificCate(
            @PathVariable  int cateid,
            @RequestParam int page
    ){
        try {
            Pageable pageable = PageRequest.of(page,3);
            Map map = new HashMap();
            map.put("message","success to fetch article!");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",articleRepository.findArticleByCategoryCategoryId(cateid,pageable).getContent());
            map.put("pageable",articleRepository.findArticleByCategoryCategoryId(cateid,pageable).getPageable());
            return ResponseEntity.ok().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to fetch article!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }


    // fetch article
    @GetMapping("/articles") public ResponseEntity<Map<String, Object>> getAllCategory(
            @RequestParam Optional<Integer> pagenumber)
    {
        if(pagenumber.isEmpty()) pagenumber = Optional.of(0);

        try {
            Map map = new HashMap();
            Pageable pageOfElemet = PageRequest.of(pagenumber.get(),3);

            map.put("message","success.");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",articleRepository.findAll(pageOfElemet).getContent()); // get only list of data
            map.put("pagable",articleRepository.findAll(pageOfElemet).getPageable()); // get only page

            return ResponseEntity.ok().body(map);

        } catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to fetch article!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }

    // add article
    @PostMapping("/article/add") public ResponseEntity<Map<String, Object>> addArticle(
            @RequestBody ArticleRequest articleRequest,
            @RequestParam int userid, @RequestParam int cateid)
    {
        try {
            Article article = new Article();
            article.setArticleId(((int)(Math.random() * 1000000)) % 1000);
            article.setTitle(articleRequest.getTitle());
            article.setDescription(articleRequest.getDescription());
            Users users = userRepository.findById(userid).get();
            Category category = categoryRepository.findById(cateid).get();
            article.setUsers(users);
            article.setCategory(category);
            articleRepository.save(article);

            Map map = new HashMap();
            map.put("message","success to add article.");
            map.put("code",200);
            map.put("status","OK");
            return ResponseEntity.ok().body(map);

        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to add article!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }


    // update article
    @PutMapping("/article/edit/{articleid}") public ResponseEntity<Map<String, Object>> updateArticle(
            @PathVariable int articleid,
            @RequestBody ArticleRequest articleRequest
    )
    {
        try{
            Article article = articleRepository.findById(articleid).get();
            article.setTitle(articleRequest.getTitle());
            article.setDescription(articleRequest.getDescription());
            articleRepository.save(article);
            Map map = new HashMap();
            map.put("message","article with id "+articleid+" updated.");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",article);
            return ResponseEntity.ok().body(map);

        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to update!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }

    // delete article
    @DeleteMapping("/article/delete/{articleid}") public ResponseEntity<Map<String, Object>> deleteArticle(
            @PathVariable int articleid
    )
    {
        try {
            Map map = new HashMap();
            map.put("message","article with id "+articleid+" deleted.");
            map.put("code",200);
            map.put("status","OK");
            articleRepository.deleteById(articleid);
            return ResponseEntity.ok().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to delete!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }

}
