package com.example.homeworki.controller;


import com.example.homeworki.model.Category;
import com.example.homeworki.payload.request.CategoryRequest;
import com.example.homeworki.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class CategoryRestController {

    @Autowired
    private CategoryRepository categoryRepository;

    // fetch category
    @GetMapping("/category") public ResponseEntity<Map<String, Object>> getAllCategory(
            @RequestParam Optional<Integer> pagenumber
    ){
        try{
            Pageable pageOfCategory = PageRequest.of(pagenumber.get(),3);
            List<Category> categories = categoryRepository.findAll(pageOfCategory).getContent();
            Map map = new HashMap();
            map.put("message","success to fetch categories.");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",categories);
            map.put("pageable",categoryRepository.findAll(pageOfCategory).getPageable());
            return ResponseEntity.badRequest().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to fetch category!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }


    // add category
    @PostMapping("/category/post") public ResponseEntity<Map<String, Object>> addCategory(
            @RequestBody CategoryRequest categoryRequest
    ){
        try{
            Category category = new Category();
            int num = ((int)(Math.random() * 1000000)) % 1000;
            category.setCategoryId(num);
            category.setCategoryName(categoryRequest.getCategoryname());
            categoryRepository.save(category);
            Map map = new HashMap();
            map.put("message","success to add categories.");
            map.put("code",200);
            map.put("status","OK");
            return ResponseEntity.badRequest().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to add category!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }

    // update category
    @PutMapping("/category/edit/{categoryid}")
    public ResponseEntity<Map<String, Object>> updateCategory(
            @PathVariable int categoryid,
            @RequestBody CategoryRequest categoryRequest
    ){
        try {
            Category category = categoryRepository.findById(categoryid).get();
            category.setCategoryName(categoryRequest.getCategoryname());
            categoryRepository.save(category);
            Map map = new HashMap();
            map.put("message","category with id "+categoryid+" updated.");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",category);
            return ResponseEntity.badRequest().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to update category!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }

    // delete category
    @DeleteMapping("/category/delete/{categoryid}") public ResponseEntity<Map<String, Object>> deleteCategory(
            @PathVariable int categoryid
    ){
        try{
            categoryRepository.deleteById(categoryid);
            Map map = new HashMap();
            map.put("message","category with id "+categoryid+" deleted.");
            map.put("code",200);
            map.put("status","OK");
            return ResponseEntity.badRequest().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to delete category!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }
}
