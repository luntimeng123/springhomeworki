package com.example.homeworki.controller;


import com.example.homeworki.model.Users;
import com.example.homeworki.payload.request.UserResquest;
import com.example.homeworki.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class UserRestController {

    @Autowired
    private UserRepository userRepository;

    // fetch user
    @GetMapping("/users") public ResponseEntity<Map<String, Object>> getUsersById(
            @RequestParam Optional<Integer> pagenumber
    ){
        if (pagenumber.isEmpty()) pagenumber = Optional.of(0);

        try {
            Pageable pageOfUsers = PageRequest.of(pagenumber.get(),3);
            List<Users> users = userRepository.findAll(pageOfUsers).getContent();
            Map map = new HashMap();
            map.put("message","success to fetch users.");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",users);
            map.put("pageable",userRepository.findAll(pageOfUsers).getPageable());
            return ResponseEntity.badRequest().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to fetch users!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }


    // fetch user by roleid
    @GetMapping("/users/role/{roleid}") public ResponseEntity<Map<String, Object>> getUsersByRoleid(
            @PathVariable int roleid,
            @RequestParam Optional<Integer> pagenumber
    ){
        if (pagenumber.isEmpty()) pagenumber = Optional.of(0);
        try{
            Pageable pageOfUsers = PageRequest.of(pagenumber.get(),3);
            Page<Users> users = userRepository.findUsersByRolesRolesId(roleid,pageOfUsers);
            Map map = new HashMap();
            map.put("message","success to fetch users.");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",users.getContent());
            map.put("pageable",users.getPageable());
            return ResponseEntity.badRequest().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to fetch users!");
            map.put("code",400);
            map.put("status","BadRequest");
            return ResponseEntity.badRequest().body(map);
        }
    }

    // add user
    @PostMapping("/users/add/{roleid}") public ResponseEntity<Map<String, Object>> addUser(
            @RequestBody UserResquest userResquest,
            @PathVariable int roleid
    ){
     try {
         Users users = new Users();
         int num = ((int)(Math.random() * 1000000)) % 1000;
         users.setUserId(num);
         users.setUserName(userResquest.getUsername());
         users.setPassWord(userResquest.getPassword());
         userRepository.save(users);

         userRepository.addUsersRoles(num,roleid);

         Map map = new HashMap();
         map.put("message","success to add users.");
         map.put("code",200);
         map.put("status","OK");
         return ResponseEntity.badRequest().body(map);

     }catch (Exception e){
         Map map = new HashMap();
         map.put("message","fails to add users!");
         map.put("code",400);
         map.put("status","BadRequest");
         System.out.println("e = " + e);
         return ResponseEntity.badRequest().body(map);
     }
    }

    // delete Users
    @DeleteMapping("/users/delete/{userid}") public ResponseEntity<Map<String, Object>> deleteUsers(
            @PathVariable int userid
    ){
        try {
            userRepository.deleteById(userid);
            userRepository.deleteUserRoles(userid);
            Map map = new HashMap();
            map.put("message","user with id "+userid+" deleted");
            map.put("code",200);
            map.put("status","OK");
            return ResponseEntity.badRequest().body(map);
        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to delete!");
            map.put("code",400);
            map.put("status","BadRequest");
            System.out.println("e = " + e);
            return ResponseEntity.badRequest().body(map);
        }
    }


    //update Users
    @PutMapping("users/edit/{userid}")
    public ResponseEntity<Map<String, Object>> updateUser(
            @PathVariable int userid,
            @RequestBody UserResquest userResquest
    ){
        try{
            Users users = userRepository.findById(userid).get();
            users.setUserName(userResquest.getUsername());
            users.setPassWord(userResquest.getPassword());
            userRepository.save(users);
            Map map = new HashMap();
            map.put("message","user with id "+userid+" updated");
            map.put("code",200);
            map.put("status","OK");
            map.put("data",users);
            return ResponseEntity.badRequest().body(map);

        }catch (Exception e){
            Map map = new HashMap();
            map.put("message","fails to update!");
            map.put("code",400);
            map.put("status","BadRequest");
            System.out.println("e = " + e);
            return ResponseEntity.badRequest().body(map);
        }
    }




}
