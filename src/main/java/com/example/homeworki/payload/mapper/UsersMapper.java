package com.example.homeworki.payload.mapper;


import com.example.homeworki.model.Users;
import com.example.homeworki.payload.dto.UserDto;
import org.mapstruct.Mapper;


@Mapper(
        componentModel = "spring"
)
public interface UsersMapper {

    UserDto userToUserDto(Users users);
    Users userDtoToUser(UserDto userDto);
}
