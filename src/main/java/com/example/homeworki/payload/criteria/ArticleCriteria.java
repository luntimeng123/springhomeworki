package com.example.homeworki.payload.criteria;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleCriteria {

    private long cetegoryId;
    private String userName;
}
