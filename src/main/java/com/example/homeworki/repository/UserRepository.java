package com.example.homeworki.repository;


import com.example.homeworki.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository public interface UserRepository extends JpaRepository<Users, Integer> {

    @Transactional
    @Modifying
    @Query(value = "insert into users_roles(user_id, role_id) VALUES (:uId,:rId)"
            ,nativeQuery = true)
    void addUsersRoles(int uId,int rId);

    @Transactional
    @Modifying
    @Query(value = "delete from users_roles where user_id = :uId",nativeQuery = true)
    void deleteUserRoles(int uId);

    // find user by role
    Page<Users> findUsersByRolesRolesId(int roleid, Pageable pageable);

}
