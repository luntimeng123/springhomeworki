package com.example.homeworki.repository.spec;


import com.example.homeworki.model.Article;
import com.example.homeworki.model.Category;
import com.example.homeworki.model.Users;
import com.example.homeworki.payload.criteria.ArticleCriteria;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class AritcleSpecification implements Specification<Article> {

    private ArticleCriteria articleCriteria;

    @Override public Predicate toPredicate(
            Root<Article> root,
            CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder
    ) {

        List<Predicate> predicates = new ArrayList<>();
        // from
        Join<Article, Category> join = root.join("category",JoinType.INNER);
        Join<Article, Users> join1 = root.join("users",JoinType.INNER);
        // where
        predicates.add(criteriaBuilder.equal(join.get("categoryId"),articleCriteria.getCetegoryId()));
        predicates.add(criteriaBuilder.like(join1.get("userName"),
                "%"+articleCriteria.getUserName()+"%"));

//        Predicate predicate = criteriaBuilder.equal(join.get("categoryId"), articleCriteria.getCetegoryId());
//        Predicate predicate1 = criteriaBuilder.like(join1.get("userName"),articleCriteria.getUserName());

        Predicate[] predicates1 = new Predicate[predicates.size()];

        if (articleCriteria.getCetegoryId() == 0 && articleCriteria.getUserName() == null) return null;

        else if (articleCriteria.getCetegoryId() == 0 && articleCriteria.getUserName() != null){
            return criteriaBuilder.like(join1.get("userName"), "%"+articleCriteria.getUserName()+"%");
        } else if (articleCriteria.getCetegoryId() != 0 && articleCriteria.getUserName() == null){
            return criteriaBuilder.equal(join.get("categoryId"),articleCriteria.getCetegoryId());
        } else {
            return criteriaBuilder.and(predicates.toArray(predicates1));
        }
    }

}
