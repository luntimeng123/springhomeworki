package com.example.homeworki.repository;


import com.example.homeworki.model.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer>,
        PagingAndSortingRepository<Article, Integer>, JpaSpecificationExecutor<Article> {

        Page<Article> findArticleByUsersUserId(int userid, Pageable pageable);
        Page<Article> findArticleByCategoryCategoryId(int cateid, Pageable pageable);

}
